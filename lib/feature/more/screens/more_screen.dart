import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tecky_chat/feature/common/widgets/custom_bottom_navigation_bar.dart';
import 'package:tecky_chat/feature/more/modals/account_info.dart';
import 'package:tecky_chat/feature/more/widgets/account_info_option_item.dart';
import 'package:tecky_chat/feature/more/widgets/option_item.dart';

import '../modals/option.dart';

class MoreScreen extends StatefulWidget {
  MoreScreen({Key? key}) : super(key: key);

  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  final _accountInfoOption = AccountInfoOption(
      userName: "Almyra Zamzamy",
      telNumber: "85253732650",
      imageURL: "https://picsum.photos/50/50");

  final _options = [
    Option(
        title: "Account",
        logo: Icon(Icons.person_outline),
        pathName: "account"),
    Option(
        title: "Chats",
        logo: Icon(CupertinoIcons.chat_bubble),
        pathName: "chats"),
    Option(
        title: "Appearance",
        logo: Icon(Icons.light_mode_outlined),
        pathName: "appearance"),
    Option(
        title: "Notification",
        logo: Icon(Icons.person),
        pathName: "notification"),
    Option(
        title: "Privacy",
        logo: Icon(Icons.shield_outlined),
        pathName: "privacy"),
    Option(
        title: "Data Usage",
        logo: Icon(Icons.folder_outlined),
        pathName: "data_usage"),
    Option(title: "Help", logo: Icon(Icons.help_outline), pathName: "help"),
    Option(
        title: "Invite Your Friends",
        logo: Icon(Icons.mail_outline),
        pathName: "invite_your_friends"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "More",
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Icon(
                Icons.add,
                color: Colors.black,
              ),
            ],
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 20,
            ),
            AccountInfoOptionItem(
                userName: _accountInfoOption.userName,
                telNumber: _accountInfoOption.telNumber,
                imageURL: _accountInfoOption.imageURL),
            SizedBox(
              height: 20,
            ),
            for (var _option in _options)
              OptionItem(
                  title: _option.title,
                  logo: _option.logo,
                  pathName: _option.pathName),
            Expanded(child: Text("")),
            CustomBottomNavigationBar(),
          ],
        ));
  }
}
