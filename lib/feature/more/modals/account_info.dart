class AccountInfoOption {
  final String userName;
  final String telNumber;
  final String imageURL;

  AccountInfoOption({
    required this.userName,
    required this.telNumber,
    required this.imageURL,
  });
}
