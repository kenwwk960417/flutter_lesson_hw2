import 'package:flutter/cupertino.dart';

class Option {
  final Icon logo;
  final String title;
  final String pathName;

  Option({required this.title, required this.logo, required this.pathName});
}
