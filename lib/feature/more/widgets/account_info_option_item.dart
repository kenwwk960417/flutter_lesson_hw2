import 'package:flutter/material.dart';
import "package:go_router/go_router.dart";

class AccountInfoOptionItem extends StatefulWidget {
  final String userName;
  final String telNumber;
  final String imageURL;

  AccountInfoOptionItem({
    required this.userName,
    required this.telNumber,
    required this.imageURL,
  });

  @override
  State<AccountInfoOptionItem> createState() => _AccountInfoOptionItemState();
}

class _AccountInfoOptionItemState extends State<AccountInfoOptionItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => context.go("/account_info"),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                SizedBox(width: 20),
                CircleAvatar(
                  backgroundImage: NetworkImage(widget.imageURL),
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.userName,
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      '+${widget.telNumber}',
                      style: TextStyle(
                        color: Color(0x5F0F1828),
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Icon(
                  Icons.chevron_right,
                  color: Color(0x5F0F1828),
                ),
                SizedBox(
                  width: 20,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
