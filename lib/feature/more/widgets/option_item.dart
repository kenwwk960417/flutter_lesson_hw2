import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tecky_chat/feature/more/modals/option.dart';
import 'package:go_router/go_router.dart';

class OptionItem extends StatefulWidget {
  final String title;
  final Icon logo;
  final String pathName;

  OptionItem({required this.title, required this.logo, required this.pathName});

  @override
  State<OptionItem> createState() => _OptionItemState();
}

class _OptionItemState extends State<OptionItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => context.go("/${widget.pathName}"),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Row(
            children: [
              widget.logo,
              const SizedBox(
                width: 6,
              ),
              Text(
                widget.title,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Row(
            children: const [
              Icon(
                Icons.chevron_right,
                color: Color(0x5F0F1828),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
