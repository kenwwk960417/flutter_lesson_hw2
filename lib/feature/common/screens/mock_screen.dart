import 'package:flutter/material.dart';
import 'package:tecky_chat/feature/common/widgets/custom_bottom_navigation_bar.dart';
import 'package:go_router/go_router.dart';

class MockScreen extends StatelessWidget {
  final String title;

  const MockScreen({required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => context.go('/more'),
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              title,
              style: TextStyle(color: Colors.black),
            ),
          ],
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(child: Text(title)),
            CustomBottomNavigationBar(),
          ],
        ),
      ),
    );
  }
}
