import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:go_router/go_router.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  CustomBottomNavigationBar({Key? key}) : super(key: key);

  @override
  State<CustomBottomNavigationBar> createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  num currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    String location = GoRouter.of(context).location;
    String preLocation = "";

    if (location != preLocation) {
      setState(() {
        print(location);
        switch (location) {
          case "/contact":
            currentIndex = 1;
            break;
          case "/chats":
            currentIndex = 2;
            break;
          case "/more":
            currentIndex = 3;
            break;
          default:
            currentIndex = 0;
        }
      });
    }

    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.04),
          offset: Offset(0, -1),
          spreadRadius: 24,
        )
      ]),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      currentIndex = 1;
                    });
                    context.go("/contact");
                  },
                  child: currentIndex == 1
                      ? Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Text("Contacts"),
                              SizedBox(
                                height: 6,
                              ),
                              Container(
                                width: 5,
                                height: 5,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ))
                      : Icon(Icons.group)),
            ),
            Expanded(
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      currentIndex = 2;
                    });
                    context.go("/chats");
                  },
                  child: currentIndex == 2
                      ? Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Text("Chats"),
                              SizedBox(
                                height: 6,
                              ),
                              Container(
                                width: 5,
                                height: 5,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ))
                      : Icon(CupertinoIcons.chat_bubble)),
            ),
            Expanded(
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      currentIndex = 3;
                    });
                    context.go("/more");
                  },
                  child: currentIndex == 3
                      ? Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Text("More"),
                              SizedBox(
                                height: 6,
                              ),
                              Container(
                                width: 5,
                                height: 5,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ))
                      : Icon(Icons.more_horiz)),
            ),
          ],
        ),
      ),
    );
  }
}
