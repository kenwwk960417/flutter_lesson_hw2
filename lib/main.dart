import 'package:flutter/material.dart';
import 'package:tecky_chat/feature/common/screens/mock_screen.dart';
import 'package:tecky_chat/feature/more/screens/more_screen.dart';
import 'package:go_router/go_router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final _router = GoRouter(initialLocation: "/", routes: [
    GoRoute(path: "/", redirect: (_) => '/more'),
    GoRoute(
      path: "/chats",
      builder: (context, state) => MockScreen(title: "Chats"),
    ),
    GoRoute(
      path: "/appearance",
      builder: (context, state) => MockScreen(title: "Appearance"),
    ),
    GoRoute(
      path: "/notification",
      builder: (context, state) => MockScreen(title: "Notification"),
    ),
    GoRoute(
      path: "/privacy",
      builder: (context, state) => MockScreen(title: "Privacy"),
    ),
    GoRoute(
      path: "/data_usage",
      builder: (context, state) => MockScreen(title: "Data Usage"),
    ),
    GoRoute(
      path: "/help",
      builder: (context, state) => MockScreen(title: "Help"),
    ),
    GoRoute(
      path: "/invite_your_friends",
      builder: (context, state) => MockScreen(title: "Invite Your Friends"),
    ),
    GoRoute(
      path: "/account",
      builder: (context, state) => MockScreen(title: "Account"),
    ),
    GoRoute(
      path: "/account_info",
      builder: (context, state) => MockScreen(title: "Account Info"),
    ),
    GoRoute(
      path: "/more",
      builder: (context, state) => MoreScreen(),
    ),
    GoRoute(
      path: "/contact",
      builder: (context, state) => MockScreen(
        title: "Contact",
      ),
    ),
  ]);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: _router.routeInformationParser,
      routerDelegate: _router.routerDelegate,
      title: "Tecky Chat",
    );
  }
}
